""" Streaming sensors reading and handling """

import serial

from datetime import datetime
import collections
import json

from google.cloud import pubsub_v1
project_id = "ariot-icrib"
topic_name = "sensorstream"

publisher = pubsub_v1.PublisherClient()

topic_path = publisher.topic_path(project_id, topic_name)


# for n in range(1, 10):
#     data = u'Message number {}'.format(n)
#     # Data must be a bytestring
#     data = data.encode('utf-8')
#     # When you publish a message, the client returns a future.
#     future = publisher.publish(topic_path, data=data)
#     print('Published {} of message ID {}.'.format(data, future.result()))
#
# print('Published messages.')
dict = collections.defaultdict(dict)
while 1:
    with serial.Serial('/dev/cu.usbmodem144101', baudrate=9600, timeout=10) as ser:
        line = ser.readline()
        split_line = line.split()

        dict['time'] = str(datetime.now())
        print(split_line)
        # print(len(split_line))
        idx = 0
        for i in range(int(len(split_line)/2)):

            dict['sensor'] = str(split_line[idx].decode().strip(':'))
            idx += 1
            dict['value'] = float(split_line[idx].decode())
            idx += 1
            msg = json.dumps(dict)
            msg = msg.encode('utf-8')
            future = publisher.publish(topic_path, data=msg)
            print('Published {} of message ID {}.'.format(msg, future.result()))
        #
        # # Humidity
        # dict['sensor'] = str(split_line[0].decode().strip(':'))
        # # print(split_line[0].decode().strip(':'))
        # dict['value'] = float(split_line[1].decode())
        # msg = json.dumps(dict)
        # msg = msg.encode('utf-8')
        # future = publisher.publish(topic_path, data=msg)
        #
        # print('Published {} of message ID {}.'.format(msg, future.result()))
        #
        # # Humidity
        # dict['sensor'] = str(split_line[3].decode().strip(':'))
        # dict['value'] = float(split_line[4].decode())
        # msg = json.dumps(dict)
        # msg = msg.encode('utf-8')
        # future = publisher.publish(topic_path, data=msg)
        #
        # print('Published {} of message ID {}.'.format(msg, future.result()))
