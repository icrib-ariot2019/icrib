// Example testing sketch for various DHT humidity/temperature sensors
// Modified from example by ladyada, public domain

#include "DHT.h"
#include <OneWire.h>
#include <DallasTemperature.h>

#define DHTPIN 2     // what pin we're connected to

// Data wire is plugged into pin 2 on the Arduino
#define ONE_WIRE_BUS 4

// Water sensor
int DA=A0;  // Pin for Analog Output - AO
int sensorvalue = 0;

// Setup a oneWire instance to communicate with any OneWire devices 
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11 
//#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

DHT dht(DHTPIN, DHTTYPE);

void setup() 
{

    Serial.begin(9600); 
    // Serial.println("DHTxx test!");

    /*if using WIO link,must pull up the power pin.*/
    // pinMode(PIN_GROVE_POWER, OUTPUT);
    // digitalWrite(PIN_GROVE_POWER, 1);

    dht.begin();
    sensors.begin();
}

void loop() 
{
    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    
    Serial.print("Humidity: "); 
    Serial.print(h);
    Serial.print(" Temperature: "); 
    Serial.print(t);


    // From temperature probe

    sensors.requestTemperatures(); // Send the command to get temperatures
    Serial.print(" Temp_Probe: ");
    Serial.print(sensors.getTempCByIndex(0)); // Why "byIndex"? 
    // You can have more than one IC on the same bus. 
    // 0 refers to the first IC on the wire


    // From the water sensor
    int sensorvalue = analogRead(DA);  //Read the analog value
    Serial.print(" Water_level: ");  
    Serial.println(sensorvalue);  //Print the analog value
    
    delay(1000);
}
