## SQL stuff-> not time to proper handling
# create table temperatureLog (userId VARCHAR(20), temperature FLOAT, eventTime DATETIME);
# CREATE TABLE toiletVisits (userId VARCHAR(20), action VARCHAR(20), eventTime DATETIME);
# create table personalia (userId VARCHAR(20), name VARCHAR(40), age INT, gender VARCHAR(20), occupation VARCHAR(20),country VARCHAR(20), height FLOAT, weight FLOAT);


create table personalia (userId VARCHAR(20), pressure FLOAT, eventTime DATETIME);

## PERSONS:
#INSERT INTO personalia VALUES('user1234', 'ROSA', 25, 'robot', 'assistant', 'denmark', 1.6, 30.0);
#INSERT INTO personalia VALUES('user1235', 'Gullik', 32, 'male', 'bank-robber', 'norway', 1.8, 74.0);
#INSERT INTO personalia VALUES('user1236', 'Angeliqa', 25, 'female', 'engineer', 'swedish', 1.62, 53);
INSERT INTO personalia VALUES('user1237', 'Joachim', 25, 'male', 'engineer', 'norway', 1.77, 67);
INSERT INTO personalia VALUES('user1238', 'Erik', 28, 'male', 'bank-robber', 'norway', 1.9, 97);
INSERT INTO personalia VALUES('user1239', 'Simen', 26, 'male', 'professiona-dancer', 'norway', 1.93, 84);


#Create ML Table
#CREATE TABLE mlInfo (userId VARCHAR(20), name VARCHAR(20), height FLOAT, weight FLOAT, gender VARCHAR(20), occupation VARCHAR(20), smile_intensity FLOAT,
#                       emotion VARCHAR(20), glasses VARCHAR(20), occlusion VARCHAR(20), sosial_grade FLOAT, skill_grade FLOAT, assigned_occupation VARCHAR(20))


CREATE VIEW temperatureLogNames
AS
SELECT  t.*, p.name
FROM    temperatureLog t
INNER JOIN personalia p
            ON t.userId = p.userId

CREATE VIEW toiletLogNames
AS
SELECT  t.*, p.name
FROM    toiletVisits t
INNER JOIN personalia p
            ON t.userId = p.userId


