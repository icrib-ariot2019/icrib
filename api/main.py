# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START app]


import logging

from flask import Flask, request, jsonify, Response
import json
import MySQLdb
import os
from datetime import datetime, timedelta

from functools import wraps
# from flask import request, Response

#
# #######################################
# ## Authentification
# #######################################
#
# def check_auth(username, password):
#     """This function is called to check if a username /
#     password combination is valid.
#     """
#     return username == 'admin' and password == 'secret'
#
# def authenticate():
#     """Sends a 401 response that enables basic auth"""
#     return Response(
#     'Could not verify your access level for that URL.\n'
#     'You have to login with proper credentials', 401,
#     {'WWW-Authenticate': 'Basic realm="Login Required"'})
#
# def requires_auth(f):
#     @wraps(f)
#     def decorated(*args, **kwargs):
#         auth = request.authorization
#         if not auth or not check_auth(auth.username, auth.password):
#             return authenticate()
#         return f(*args, **kwargs)
#     return decorated



app = Flask(__name__)

HOST = '35.228.69.77'
USER = 'admin'
PASSWD = 'babyshark'

# These environment variables are configured in app.yaml.
CLOUDSQL_CONNECTION_NAME = os.environ.get('CLOUDSQL_CONNECTION_NAME')
CLOUDSQL_USER = os.environ.get('CLOUDSQL_USER')
CLOUDSQL_PASSWORD = os.environ.get('CLOUDSQL_PASSWORD')


def connect_to_cloudsql():
    # When deployed to App Engine, the `SERVER_SOFTWARE` environment variable
    # will be set to 'Google App Engine/version'.
    if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine/'):
        # Connect using the unix socket located at
        # /cloudsql/cloudsql-connection-name.
        cloudsql_unix_socket = os.path.join(
            '/cloudsql/', CLOUDSQL_CONNECTION_NAME)

        db = MySQLdb.connect(
            unix_socket=cloudsql_unix_socket,
            user=CLOUDSQL_USER,
            passwd=CLOUDSQL_PASSWORD)

    # If the unix socket is unavailable, then try to connect using TCP. This
    # will work if you're running a local MySQL server or using the Cloud SQL
    # proxy, for example:
    #
    #   $ cloud_sql_proxy -instances=your-connection-name=tcp:3306
    #
    else:
        db = MySQLdb.connect(
            host=HOST, user=USER, passwd=PASSWD)

    return db

@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return 'iCrib - Modern family by Babyshark Ultd'


@app.errorhandler(500)
def server_error(e):
    # Log the error and stacktrace.
    logging.exception('An error occurred during a request.')
    return 'An internal error occurred.', 500

@app.route('/toilet/', methods=['GET'])
# @app.requires_auth
def toilet_info():
    """Return toilet visits for a user"""
    return 'Ackward2'

@app.route('/sensor_data/', methods=['GET'])
def get_sensor_data():

    db = connect_to_cloudsql()

    sql_select = """
        select t.sensor, t.time, t.value from arduino_data t inner join(select sensor, max(time) as maxtime from arduino_data group by sensor) tm on t.sensor=tm.sensor and t.time = tm.maxtime;
               """

    cursor = db.cursor()
    cursor.execute("USE sensor_data")
    cursor.execute(sql_select)

    results = cursor.fetchall()

    print(results)

    return jsonify(results)

## SQL stuff-> not time to proper handling
# create table temperatureLog (userId VARCHAR(20), temperature FLOAT, eventTime DATETIME);
# CREATE TABLE toiletVisits (userId VARCHAR(20), action VARCHAR(20), eventTime DATETIME);
# create table personalia (userId VARCHAR(20), name VARCHAR(40), age INT, gender VARCHAR(20), occupation VARCHAR(20),country VARCHAR(20), height FLOAT, weight FLOAT);

## PERSONS:
#INSERT INTO personalia VALUES('user1234', 'ROSA', 25, 'robot', 'assistant', 'denmark', 1.6, 30.0);
#INSERT INTO personalia VALUES('user1235', 'Gullik', 32, 'male', 'bank-robber', 'norway', 1.8, 74.0);

#Create ML Table
#CREATE TABLE mlInfo (userId VARCHAR(20), name VARCHAR(20), height FLOAT, weight FLOAT, gender VARCHAR(20), occupation VARCHAR(20), smile_intensity FLOAT,
#                       emotion VARCHAR(20), glasses VARCHAR(20), occlusion VARCHAR(20), sosial_grade FLOAT, skill_grade FLOAT, assigned_occupation VARCHAR(20))




#import json
#import MySQLdb
# import logging
#
# from flask import current_app, Flask, render_template, request
#
# app = Flask(__name__)
#






#

#
#
# @app.route('/toilet/register_visit', methods=['POST'])
# def toilet_user_registered():
#     db = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWD)
#     cursor = db.cursor()
#     cursor.execute("USE persons")
#
#     envelope = json.loads(request.data.decode('utf-8'))
#     userId = "'" + envelope['userId'] + "'"
#     operation = "'" + envelope['operation'] + "'"
#     eventTime = "'" + envelope['eventTime'] + "'"
#
#
#     sql_insert = """
#                       insert into toiletVisits Values (%s, %s, %s);
#                       """ % (userId, operation, eventTime)
#
#     print(sql_insert)
#
#     cursor.execute(sql_insert)
#
#     db.commit()
#
#     print(userId)
#     print(operation)
#     print(eventTime)
#
#     return 'Registered_visit'
#
#
# @app.route('/toilet/get_visits/<int:userId>', methods=['GET'])
# def get_toilet_visits(userId):
#     db = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWD)
#
#
#     sql_select = """
#                SELECT count(*) FROM toiletVisits where userId = %s and operation = 'Enter';
#                """ % (userId)
#
#     cursor = db.cursor()
#     cursor.execute("USE persons")
#     cursor.execute(sql_select)
#     results = cursor.fetchall()
#
#     count = results[0][0]
#     string = "%d" % count
#
#     return string
#
#
# @app.errorhandler(500)
# def server_error(e):
#     logging.exception('An error occurred during a request.')
#     return """
#     An internal error occurred: <pre>{}</pre>
#     See logs for full stacktrace.
#     """.format(e), 500
#
#
if __name__ == '__main__':
    # # This is used when running locally. Gunicorn is used to run the
    # # application on Google App Engine. See entrypoint in app.yaml.
    # db = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWD)
    #
    #
    # sql_insert = """
    #             insert into toiletVisits Values ('8', 'exit',  NULL);
    #             """
    #
    # sql_select = """
    #         SELECT * FROM toiletVisits;
    #         """
    #
    # cursor = db.cursor()
    # cursor.execute("USE persons")
    # cursor.execute(sql_insert)
    # db.commit()
    # #
    #
    # cursor.close()

    app.run(host='127.0.0.1', port=8080, debug=True)
# [END app]
