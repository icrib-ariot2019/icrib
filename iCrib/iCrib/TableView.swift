//
//  TableView.swift
//  iCrib
//
//  Created by Balder Riiser Haugerud on 08/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//
import UIKit

extension ViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sensor_data.asArray(metric: metric).count / 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! CustomTableViewCell
        cell.column1top.text = "\(self.sensor_data.asArray(metric: metric)[indexPath.row * 4])"
        cell.column1btm.text = "\(self.sensor_data.asArray(metric: metric)[indexPath.row * 4 + 1])"
        cell.column2top.text = "\(self.sensor_data.asArray(metric: metric)[indexPath.row * 4 + 2])"
        cell.column2btm.text = "\(self.sensor_data.asArray(metric: metric)[indexPath.row * 4 + 3])"
        if (indexPath.row == 0) {
            allCells = []
        }
        allCells.append(cell)
        
        cell.column1top.textColor = selectedColor
        cell.column2top.textColor = selectedColor
        
        if (cell.column1top.text! == "Temperature") {
            if (self.sensor_data.temperatureLimit()) {
                cell.column1btm.textColor = .black
            } else {
                cell.column1btm.textColor = .red
            }
        }
        
        if (cell.column1top.text! == "Baby temperature") {
            if (self.sensor_data.tempProbeLimit()) {
                cell.column1btm.textColor = .black
            } else {
                cell.column1btm.textColor = .red
            }
        }
        
        if (cell.column1top.text! == "Baby cry") {
            if let baby_cry = self.sensor_data.baby_cry {
                if (baby_cry != 9.0) {
                    cell.column1btm.textColor = .red
                } else {
                    cell.column1btm.textColor = .black
                }
            } else {
                cell.column1btm.textColor = .black
            }
        }
        
        if (cell.column2top.text! == "Humidity") {
            if (self.sensor_data.humidityLimit()) {
                cell.column2btm.textColor = .black
            } else {
                cell.column2btm.textColor = .red
            }
        }
        
        if (cell.column2top.text! == "Wet accident") {
            if let water = self.sensor_data.water {
                if water {
                    cell.column2btm.textColor = .red
                } else {
                    cell.column2btm.textColor = .black
                }
            } else {
                cell.column2btm.textColor = .black
            }
        }
        cell.backgroundColor = UIColor(displayP3Red: CGFloat(241.0 / 255.0), green: CGFloat(110.0 / 255.0), blue: CGFloat(154.0 / 255.0), alpha: 0.02)
        
        return cell
    }
    
    func addTableView() {
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "MyCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 66
        tableView.backgroundColor = UIColor(displayP3Red: CGFloat(241.0 / 255.0), green: CGFloat(110.0 / 255.0), blue: CGFloat(154.0 / 255.0), alpha: 0.02)
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(updateSensorData(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
        self.view.addSubview(tableView)
        setTableViewColors()
    }
    
    func setTableViewColors() {
        for cell in allCells {
            cell.column1top.textColor = selectedColor
            cell.column2top.textColor = selectedColor
        }
    }
    
    @objc private func updateSensorData(_ sender: Any) {
        // Fetch Sensor Data
        setSensorData();
    }
}
