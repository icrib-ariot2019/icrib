//
//  ViewController.swift
//  iCrib
//
//  Created by Balder Riiser Haugerud on 07/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var webView: WKWebView!
    var camButton: UISwitch!
    var dayText: UILabel!
    var nightText: UILabel!
    var switchButton: UISwitch!
    var refreshControl: UIRefreshControl!
    var tableView: UITableView!
    var horizontalLine: UIView!
    var metricText: UILabel!
    var imperialText: UILabel!
    var babyHappyImg: UIImageView!
    var babySharkImgLeft: UIImageView!
    var babySharkImgRight: UIImageView!
    
    let SENSOR_DATA_URL = "https://ariot-icrib.appspot.com/sensor_data/" // TODO: INSERT YOUR API URL HERE
    let DAY_CAM_URL = URL(string: "http://192.168.100.250:8000")! // TODO: INSERT YOUR DAY CAM URL HERE
    let NIGHT_CAM_URL = URL(string: "http://192.168.100.77:8000")! // TODO: INSERT YOUR NIGHT CAM URL HERE
    let ROWS = 3
    var metric = true
    var isDay = true
    var sensor_data = SensorData();
    var selectedColor = UIColor(displayP3Red: CGFloat(241.0 / 255.0), green: CGFloat(110.0 / 255.0), blue: CGFloat(154.0 / 255.0), alpha: 1.0)
    var allCells: [CustomTableViewCell] = []
    
    // Screen width.
    public var screenWidth: Int {
        return Int(UIScreen.main.bounds.width)
    }
    
    // Screen height.
    public var screenHeight: Int {
        return Int(UIScreen.main.bounds.height)
    }
    
    override func loadView() {
        webView = WKWebView()
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSensorData()
        // Web view
        addWebView()
        // Cam Button
        dayText = UILabel(frame:CGRect(x: 70, y: 348, width: 80, height: 20))
        nightText = UILabel(frame:CGRect(x: 220, y: 348, width: 80, height: 20))
        camButton = UISwitch(frame:CGRect(x: 160, y: 343, width: 1, height: 20))
        addUglyButton()
        //Metric button
        imperialText = UILabel(frame:CGRect(x: 220, y: 398, width: 80, height: 20))
        metricText = UILabel(frame:CGRect(x: 70, y: 398, width: 80, height: 20))
        switchButton = UISwitch(frame:CGRect(x: 160, y: 393, width: 1, height: 20))
        addMetricButton()
        //Babysharks
        babySharkImgLeft = UIImageView(frame: CGRect(x: 10, y: 340, width: 100, height: 100))
        babySharkImgRight = UIImageView(frame: CGRect(x: 270, y: 340, width: 100, height: 100))
        addBabySharks()
        //Horizontal Line
        horizontalLine = UIView(frame: CGRect(x: 40, y: 445, width: screenWidth - 80, height: 2))
        horizontalLine.backgroundColor = selectedColor
        self.view.addSubview(horizontalLine)
        //Table view (w RefreshControl)
        tableView = UITableView(frame: CGRect(x: 2, y: 470, width: screenWidth - 4, height: 66 * ROWS + 2))
        addTableView()
        // Baby happy
        babyHappyImg = UIImageView(frame: CGRect(x: screenWidth / 2 - 60, y: 678, width: 120, height: 120))
        addBabyHappy()
    }
    
    func setSensorData(){
        //creating a URL
        let url = URL(string: SENSOR_DATA_URL)
        
        //fetching the data from the url
        URLSession.shared.dataTask(with: (url)!, completionHandler: {(data, response, error) -> Void in
            
            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSArray {
                
                let new_sensor_data_obj = SensorData()
                for sensor_value in jsonObj! {
                    let value_array = (sensor_value as! NSArray);
                    if ((value_array[0] as! String).isEqual("Temperature:")) {
                        new_sensor_data_obj.setTemperature(temperature: (value_array[2] as! Double))
                    }
                    if ((value_array[0] as! String).isEqual("Humidity:")) {
                        new_sensor_data_obj.setHumidity(humidity: (value_array[2] as! Double))
                    }
                    if ((value_array[0] as! String).isEqual("Temp_Probe:")) {
                        new_sensor_data_obj.setTempProbe(temp_probe: (value_array[2] as! Double))
                    }
                    if ((value_array[0] as! String).isEqual("Water_level:")) {
                        new_sensor_data_obj.setWater(water: !(value_array[2] as! Double).isEqual(to: 0.0))
                    }
                    if ((value_array[0] as! String).isEqual("poo_detector")) {
                        new_sensor_data_obj.setPoo(poo: !(value_array[2] as! Double).isEqual(to: 1.0))
                    }
                    if ((value_array[0] as! String).isEqual("baby_cry")) {
                        new_sensor_data_obj.setBabyCry(baby_cry: value_array[2] as! Double)
                    }
                }
                new_sensor_data_obj.setAdorable(adorable: true)
                self.sensor_data = new_sensor_data_obj
                
                OperationQueue.main.addOperation({
                    //calling another function after fetching the json
                    //it will show the names to label
                    self.tableView.reloadData()
                    self.checkBabyHappy()
                    self.refreshControl.endRefreshing()
                })
            }
        }).resume()
    }
}

