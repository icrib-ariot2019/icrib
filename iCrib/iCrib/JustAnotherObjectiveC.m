//
//  JustAnotherObjectiveC.m
//  iCrib
//
//  Created by Balder Riiser Haugerud on 08/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//

#import "JustAnotherObjectiveC.h"

@implementation JustAnotherObjectiveC
- (id) initWithFahrenheit:(double) f
{
    self = [super init];
    if (!self) return nil;
    celsius = (f - 32)*(5.0/9.0);
    return self;
}

- (id) initWithCelsius:(double) c
{
    self = [super init];
    if (!self) return nil;
    celsius = c;
    return self;
}

- (double) fahrenheitValue
{
    return (celsius*(9.0/5.0)) + 32;
}

- (double) celsiusValue
{
    return celsius;
}

@end
