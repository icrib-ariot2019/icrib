//
//  CustomTableViewCell.swift
//  iCrib
//
//  Created by Balder Riiser Haugerud on 08/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//
import Foundation
import UIKit

class CustomTableViewCell: UITableViewCell {
    let column1top = UILabel()
    let column1btm = UILabel()
    let column2top = UILabel()
    let column2btm = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        column1top.translatesAutoresizingMaskIntoConstraints = false
        column1btm.translatesAutoresizingMaskIntoConstraints = false
        column2top.translatesAutoresizingMaskIntoConstraints = false
        column2btm.translatesAutoresizingMaskIntoConstraints = false
        
        column1btm.font = UIFont.boldSystemFont(ofSize: 30.0)
        column2btm.font = UIFont.boldSystemFont(ofSize: 30.0)
    
        contentView.addSubview(column1top)
        contentView.addSubview(column1btm)
        contentView.addSubview(column2top)
        contentView.addSubview(column2btm)
        
        let viewsDict = [
            "column1top" : column1top,
            "column1btm" : column1btm,
            "column2top" : column2top,
            "column2btm" : column2btm,
            ] as [String : Any]
        
        column1top.textAlignment = .left
        column1btm.textAlignment = .left
        column2top.textAlignment = .right
        column2btm.textAlignment = .right
        column1btm.topAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        column1top.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        column2btm.topAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        column2top.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[column1top]", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(20)-[column1btm]-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[column1btm]-[column2btm]-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[column1top]-[column2top]-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[column2top]", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(20)-[column2btm]-|", options: [], metrics: nil, views: viewsDict))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
