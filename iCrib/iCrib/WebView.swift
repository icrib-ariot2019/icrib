//
//  WebView.swift
//  iCrib
//
//  Created by Balder Riiser Haugerud on 08/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//
import UIKit

extension ViewController {
    
    func addWebView() {
        webView.load(URLRequest(url: DAY_CAM_URL))
        webView.scrollView.bounces = false
        webView.scrollView.isScrollEnabled = false
    }
}
