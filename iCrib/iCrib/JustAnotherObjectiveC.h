//
//  JustAnotherObjectiveC.h
//  iCrib
//
//  Created by Balder Riiser Haugerud on 08/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface JustAnotherObjectiveC : NSObject
    {
        double celsius; // only store one temperature
    }
    
- (id) initWithFahrenheit:(double) f;
- (id) initWithCelsius:(double) c;
    
- (double) fahrenheitValue;
- (double) celsiusValue;
@end
