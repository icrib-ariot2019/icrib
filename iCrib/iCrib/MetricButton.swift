//
//  MetricButton.swift
//  iCrib
//
//  Created by Balder Riiser Haugerud on 08/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//
import UIKit

extension ViewController {
    
    func addMetricButton(){
        switchButton.addTarget(self, action: #selector(ViewController.switchValueDidChange(_:)), for: .valueChanged)
        self.view.addSubview(switchButton)
        metricText.text = "Metric"
        metricText.textAlignment = .right
        self.view.addSubview(metricText)
        imperialText.text = "Imperial"
        self.view.addSubview(imperialText)
        setMetricButtonColors()
    }
    
    func setMetricButtonColors() {
        metricText.textColor = selectedColor
        imperialText.textColor = selectedColor
        switchButton.tintColor = selectedColor
        switchButton.onTintColor = selectedColor
        
    }
    
    @objc private func switchValueDidChange(_ sender:UISwitch!) {
        if(metric) {
            metric = false
        } else {
            metric = true
        }
        self.tableView.reloadData()
    }
}
