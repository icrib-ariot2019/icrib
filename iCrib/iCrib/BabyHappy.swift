//
//  BabyHappy.swift
//  iCrib
//
//  Created by Balder Riiser Haugerud on 09/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//
import UIKit

extension ViewController {
    func addBabyHappy() {
        checkBabyHappy()
        self.view.addSubview(babyHappyImg)
    }
    
    func checkBabyHappy() {
        if self.sensor_data.checkAll() {
            let image: UIImage = UIImage(named: "check")!
            babyHappyImg.image = image
        } else {
            let image: UIImage = UIImage(named: "warning")!
            babyHappyImg.image = image
        }
    }
}
