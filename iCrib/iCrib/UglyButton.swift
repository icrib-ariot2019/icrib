//
//  UglyButton.swift
//  iCrib
//
//  Created by Balder Riiser Haugerud on 08/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//
import UIKit

extension ViewController {
    func addUglyButton() {
        camButton.setOn(!isDay, animated: false)
        camButton.addTarget(self, action: #selector(ViewController.buttonAction(_:)), for: .valueChanged)
        self.view.addSubview(camButton)
        dayText.text = "Day"
        dayText.textAlignment = .right
        self.view.addSubview(dayText)
        nightText.text = "Night"
        self.view.addSubview(nightText)
        setUglyButtonColors()
    }
    
    func setUglyButtonColors() {
        camButton.tintColor = selectedColor
        camButton.onTintColor = selectedColor
        dayText.textColor = selectedColor
        nightText.textColor = selectedColor
    }
    
    @objc func buttonAction(_ sender:UISwitch!) {
        if(isDay) {
            webView.load(URLRequest(url: NIGHT_CAM_URL))
            isDay = false
        } else {
            webView.load(URLRequest(url: DAY_CAM_URL))
            isDay = true
        }
    }
}
