//
//  BabyShark.swift
//  iCrib
//
//  Created by Balder Riiser Haugerud on 09/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//
import UIKit

extension ViewController {
    func addBabySharks() {
        let imageLeft: UIImage = UIImage(named: "babyshark")!
        babySharkImgLeft.image = imageLeft
        self.view.addSubview(babySharkImgLeft)
        let imageRight: UIImage = UIImage(named: "babyshark")!
        babySharkImgRight.image = imageRight.withHorizontallyFlippedOrientation()
        self.view.addSubview(babySharkImgRight)
    }
}
