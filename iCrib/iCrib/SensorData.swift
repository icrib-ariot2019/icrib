//
//  SensorData.swift
//  iCrib
//
//  Created by Balder Riiser Haugerud on 07/03/2019.
//  Copyright © 2019 ModernFamily by Babyshark Ultd. All rights reserved.
//
import UIKit

class SensorData: NSObject {
    
    //properties of a sensor data
    var humidity: Double?
    var water: Bool?
    var poo: Bool?
    var baby_cry: Double?
    var adorable: Bool?
    
    var temperature_converter: JustAnotherObjectiveC?
    var temp_probe_converter: JustAnotherObjectiveC?
    
    
    //empty constructor
    override init(){
    }
    
    func setTemperature(temperature: Double) {
        self.temperature_converter = JustAnotherObjectiveC.init(celsius: temperature);
    }
    
    func temperatureLimit() -> Bool {
        if let temperature_converter_var = temperature_converter {
            if temperature_converter_var.celsiusValue() < 22 || temperature_converter_var.celsiusValue() > 26 {
                return false;
            }
        }
        return true;
    }
    
    func tempProbeLimit() -> Bool {
        if let temp_probe_converter_var = temp_probe_converter {
            if temp_probe_converter_var.celsiusValue() < 36 || temp_probe_converter_var.celsiusValue() > 38 {
                return false;
            }
        }
        return true;
    }
    
    func humidityLimit() -> Bool {
        if let humidity_var = humidity {
            if humidity_var < 15 || humidity_var > 65 {
                return false;
            }
        }
        return true;
    }
    func checkAll() -> Bool {
        if let water_var = water {
            if let poo_var = poo {
                if let baby_cry_var = baby_cry {
                    return humidityLimit() && tempProbeLimit() && temperatureLimit() && !water_var && !poo_var && baby_cry_var == 9.0
                }
            }
        }
        return false
    }
    
    func setHumidity(humidity: Double) {
        self.humidity = humidity;
    }
    
    func setTempProbe(temp_probe: Double) {
        self.temp_probe_converter = JustAnotherObjectiveC.init(celsius: temp_probe);
    }
    
    func setWater(water: Bool) {
        self.water = water;
    }
    
    func setPoo(poo: Bool) {
        self.poo = poo;
    }
    
    func setBabyCry(baby_cry: Double) {
        self.baby_cry = baby_cry;
    }
    
    func setAdorable(adorable: Bool) {
        self.adorable = adorable;
    }
    
    func getTemperature(metric: Bool) -> String {
        if let temperature_converter_var = temperature_converter {
            if metric {
                return String(format:"%.1f", temperature_converter_var.celsiusValue()) + " °C";
            } else {
                return String(format:"%.1f", temperature_converter_var.fahrenheitValue()) + " °F";
            }
        } else {
            return "-";
        }
    }
    
    func getTempProbe(metric: Bool) -> String {
        if let temp_probe_converter_var = temp_probe_converter {
            if metric {
                return String(format:"%.1f", temp_probe_converter_var.celsiusValue()) + " °C";
            } else {
                return String(format:"%.1f", temp_probe_converter_var.fahrenheitValue()) + " °F";
            }
        } else {
            return "-";
        }
    }
    
    func getWater() -> String {
        if let water_var = water {
            if let poo_var = poo {
                if (water_var && poo_var) {
                    return "Mess";
                } else if (water_var && !poo_var) {
                    return "Pee";
                } else if (!water_var && poo_var) {
                    return "Poo";
                } else {
                    return "None";
                }
            } else {
                return "-";
            }
        } else {
            return "-";
        }
    }
    
    func getBabyCry() -> String {
        if let baby_cry_var = baby_cry {
            if (baby_cry_var == 0.0){
                return "Hungry";
            } else if (baby_cry_var == 1.0){
                return "Needs burping";
            } else if (baby_cry_var == 2.0){
                return "Belly pain";
            } else if (baby_cry_var == 3.0){
                return "Discomfort";
            } else if (baby_cry_var == 4.0){
                return "Tired";
            } else if (baby_cry_var == 5.0){
                return "Lonely";
            } else if (baby_cry_var == 6.0){
                return "Too cold/hot";
            } else if (baby_cry_var == 7.0){
                return "Scared";
            } else if (baby_cry_var == 8.0){
                return "Don't know";
            } else if (baby_cry_var == 9.0){
                return "No worries";
            }
        }
        return "-";
    }
    
    func getAdorable() -> String {
        if let adorable_var = adorable {
            if (adorable_var) {
                return "Yes";
            } else {
                return "No";
            }
        } else {
            return "-";
        }
    }
    
    func getHumidity() -> String {
        if let humidity_var = humidity {
            return String(format:"%.1f", humidity_var) + " %"
        } else {
            return "-";
        }
    }
    
    func asArray(metric: Bool) -> NSArray {
        return ["Temperature", getTemperature(metric: metric), "Humidity", getHumidity(), "Baby temperature", getTempProbe(metric: metric), "Accident", getWater(), "Status", getBabyCry(), "Adorable", getAdorable()];
    }
}
